### Author 
 Bart Engels

Student number: 382612

Email: b.engels@st.hanze.nl



### Containing files

Gene_seq.fasta

MSA_homologene.fasta

snp_opdracht_bin3.py


### Needed programs

Python3

#### Installing

[a link] (https://realpython.com/installing-python/#how-to-install-from-homebrew)

### Usage

-n The Single Nucleotide Polymorphisme

-p The position where the SNP replace the nucleotide in the gene sequence

-m Name of a fasta file which contains a MSA 

[GENE_SEQ_file] Name of a fasta file which contains a Geneseq


```bash
python3 bio_inf3_bart_engels.py -n [snp] -p [pos] -m [msa] [GENE_SEQ_file] 
```

