#!usr/bin/env python3

"""
This program give back a score for the conservation of a snp.
parameters
pos : position for the snp.
snp: A, G, T, C, -
msa: file with gene sequences that are aligned
gene file: file with a gene sequences where a snp is added.

output :
score : give a score between 10 and 1.
"""

__author__ = "Bart Engels"
__version__ = "v.1"
__date__ = "22-10-2020"

import random
import sys
import argparse
from Bio import AlignIO


class SNP:
    """
    this class is filed with methods that serve the purpose of giving back
    a score that resembles the difference between the GENE seq and the MSA
    """

    def __init__(self, index, snp, msa, file):
        """
        takes the input args.
        :param index: position of the snp
        :param snp: {A, G, T, C, -}
        :param msa: file with gene sequences that are aligned
        :param file: file with a gene sequences where a snp is added.
        """
        self.seq_file = AlignIO.read(file, format="fasta")
        self.msa = AlignIO.read(msa, format="fasta")
        self.pos = index
        self.nuc_seq = []
        self.snp = snp
        self.gap_list = []
        self.c_pos = 0
        self.aa_seq = ""
        self.aa_to_nuc_dict = {"A": ["GCG", "GCA", "GCC", "GCT"],
                               "C": ["TGT", "TGC"],
                               "D": ["GAC", "GAT"],
                               "E": ["GAG", "GAA"],
                               "F": ["TTT", "TTC"],
                               "G": ["GGG", "GGA", "GGC", "GGT"],
                               "H": ["CAT", "CAC"],
                               "I": ["ATT", "ATC", "ATA"],
                               "K": ["AAA", "AAG"],
                               "L": ["CTT", "CTC", "CTA", "CTG", "TTA", "TTG"],
                               "M": ["ATG"],
                               "N": ["AAT", "AAC"],
                               "P": ["CCT", "CCC", "CCA", "CCG"],
                               "Q": ["CAA", "CAG"],
                               "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
                               "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
                               "T": ["ACT", "ACC", "ACA", "ACG"],
                               "V": ["GTG", "GTA", "GTC", "GTT"],
                               "W": ["TGG"],
                               "Y": ["TAT", "TAC"],
                               "*": ["TAA", "TAG", "TGA"]
                               }
        self.nuc_to_aa_dict = {"TTT": "F", "TTC": "F", "TTA": "L", "TTG": "L",
                               "TCT": "S", "TCC": "S", "TCA": "S", "TCG": "S",
                               "TAT": "Y", "TAC": "Y", "TAA": "*", "TAG": "*",
                               "TGT": "C", "TGC": "C", "TGA": "*", "TGG": "W",
                               "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L",
                               "CCT": "P", "CCC": "P", "CCA": "P", "CCG": "P",
                               "CAT": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
                               "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R",
                               "ATT": "I", "ATC": "I", "ATA": "I", "ATG": "M",
                               "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T",
                               "AAT": "N", "AAC": "N", "AAA": "K", "AAG": "K",
                               "AGT": "S", "AGC": "S", "AGA": "R", "AGG": "R",
                               "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V",
                               "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A",
                               "GAT": "D", "GAC": "D", "GAA": "E", "GAG": "E",
                               "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G"
                               }

    def index_alignment(self):
        """
        :return: a list with the positions of the gaps from te original alinend gene sequence.
        """
        for count, amino_a in enumerate(self.seq_file[0, :]):
            if amino_a in self.aa_to_nuc_dict:
                self.nuc_seq += random.choice(self.aa_to_nuc_dict[amino_a])
            else:
                self.gap_list.append(count)

    def add_snp(self):
        """
        uses the self.pos and self.snp to make a list that is filed
        with nucleotides
        :return: a list with the new snp added
        """
        if self.pos in range(0, len(self.nuc_seq)):
            if self.snp == "-":
                self.nuc_seq[self.pos] = ""
            else:
                self.nuc_seq[self.pos] = self.snp
        else:
            print("Position is out of range. Try a number between 0 and", len(self.nuc_seq))
            sys.exit()

    def arrange_seq(self):
        """
        uses the methode dna_aa to make place the right protein on the right spot.
        and inserted gaps.
        """
        self.nuc_seq = self.aa_seq.join(self.nuc_seq)
        if len(self.seq_file[0, :]) % 3 == 0:
            for num in range(0, len(self.seq_file[0, :])):
                self.dna_aa(num)
        else:
            for num in range(0, len(self.seq_file[0, :]) - 2):
                self.dna_aa(num)

    def dna_aa(self, num):
        """
        :param num: count of self.seq_file
        hase a if else statement for what to do if num in the gap_list.
        """
        if num in self.gap_list:
            self.aa_seq += "-"
        else:
            self.aa_seq += self.nuc_to_aa_dict[self.nuc_seq[self.c_pos:self.c_pos + 3]]
            self.c_pos += 3

    def msa_score(self):
        """
        makes a score on bases of the msa on how manny of the proteins are conserved.
        :return: a massage that tells the score between 10 and 1.
        """
        index_msa_list = []
        message = ""
        percentage = 0.0
        for i in self.seq_file:
            if i.seq[int(self.pos / 3)] == self.aa_seq[int(self.pos / 3)]:
                message = " 1"
        for seq in self.msa:
            index_msa_list.append(seq.seq[int(self.pos / 3)])
        if self.aa_seq[int(self.pos / 3)] in index_msa_list:
            c_amino = index_msa_list.count(self.aa_seq[int(self.pos / 3)])
            total_a = len(index_msa_list)
            percentage = 100 / total_a * c_amino
            if percentage < 12.5:
                message = " 9"
            elif percentage < 25:
                message = " 8 "
            elif percentage < 37.5:
                message = " 7 "
            elif percentage < 50:
                message = " 6"
            elif percentage < 62.5:
                message = " 5"
            elif percentage < 75:
                message = " 4"
            elif percentage < 87.5:
                message = " 3"
            elif percentage < 100:
                message = " 2"
        elif not self.aa_seq[int(self.pos / 3)] in index_msa_list:
            message = " 10 "
        return "This snp gets a score of {},  " \
               "This score is based on a {} % presents in the MSA.".format(message, percentage)


def main(args):
    """
    use:

    python3 bio_inf3_bart_engels.py -n [snp] -p [pos] -m [msa] [GENE_SEQ_file]
    -n The Single Nucleotide Polymorphisme
    -p The position where the SNP replace the "
                             "nucleotide in the gene sequence
    -m Name of a fasta file which contains a MSA
    [GENE_SEQ_file] Name of a fasta file which contains a Geneseq
    """
    parser = argparse.ArgumentParser(description="""Predicting the effect of a
    given Single Nucleotide Polymorphism (SNP)""")

    parser.add_argument('-n', dest="snp", required=True, choices={"A", "C", "G", "T", "-"},
                        help="""The Single Nucleotide Polymorphism
             used to predict the effect of this SNP on the function of the gene""")
    parser.add_argument('-p', dest="pos", required=True, type=int,
                        help=" The position where the SNP replace the "
                             "nucleotide in the gene sequence")
    parser.add_argument('-m', dest="msa", required=True,
                        help="Name of a fasta file which contains a MSA")
    parser.add_argument(dest="file",
                        help="Name of a fasta file which contains a Geneseq")

    args = parser.parse_args()

    info = SNP(args.pos, args.snp, args.msa, args.file)
    info.index_alignment()
    info.add_snp()
    info.arrange_seq()
    print(info.msa_score())

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
